#include "Vector2f.h"

Vector2f::Vector2f()
{
	zero();
}

Vector2f::Vector2f(float x, float y)
{
	setX(x);
	setY(y);
}

Vector2f::Vector2f(Vector2f* other)
{
	// Copy constructors can access the
	// private attributes of the other
	// class!
	x = other->x;
	y = other->y;
}

Vector2f::~Vector2f()
{
}

void Vector2f::setX(float x)
{
	this->x = x;
}

float Vector2f::getX(void) 
{
	return x;
}

void Vector2f::setY(float y)
{
	this->y = y;
}

float Vector2f::getY(void)
{
	return y;
}

void Vector2f::add(Vector2f* other)
{
	x += other->getX();
	y += other->getY();
}

void Vector2f::scale(float scalar)
{
	x *= scalar;
	y *= scalar;
}

void Vector2f::zero()
{
	x = y = 0.0f;
}

float Vector2f::length()
{
	return sqrtf((x * x) + (y * y));
}

void Vector2f::normalize() {
	float length = this->length();

	if (length > 1.0f)
	{
		x /= length;
		y /= length;
	}
}

